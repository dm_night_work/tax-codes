<?php
/**
 * Created by PhpStorm.
 * User: denismusic
 * Date: 05/02/16
 * Time: 09:36
 */

require('conf.php');

if (isset($_FILES['file']['type'])) {
    $file = $_FILES['file'];
    $tmp_path = $file['tmp_name'];
}
else return false;

$db = new mysqli(HOST, USER, PASS, DB);
$csv = array_map('str_getcsv', file($tmp_path));
$size = count($csv);
$status = true;

if ($db->connect_error > 0) die('Unable to connect to database ['. $db->connect_error . ']');

for ($i = 0; $i < $size; $i++) {
    $tax = $csv[$i][0];
    $insert = "INSERT INTO tax_codes (tax) VALUES ('$tax')";

    if (!$db->query($insert)) echo false;
}

echo true;