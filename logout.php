<?php
/**
 * Created by PhpStorm.
 * User: fly
 * Date: 01/12/15
 * Time: 16:58
 */

require('conf.php');
session_start();

session_unset();

session_destroy();

header('Location:' . BASE_URL);
?>