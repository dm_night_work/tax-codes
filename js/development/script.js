/**
 * Created by fly on 01/12/15.
 */
(function(angular) {

    var app;

    app = angular.module('TaxCodesApplication', []);

    /**
     * Controllers
     */
    app.controller('SingleInputController', ['$scope', '$http', '$timeout', SingleInputController]);
    app.controller('CSVInputController', ['$scope', '$http', '$timeout', CSVInputController]);


    /**
     * Directives
     */
    app.directive('grabFile', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element.bind('change', function() {
                    scope.inputField = element;
                    scope.csvObject.file = element[0].files[0];
                    scope.fd.append('file', scope.csvObject.file, scope.csvObject.file.name);
                });
            }
        }
    });


    /**
     * FUCNTIONS - Controller functions
     */
    function SingleInputController($scope, $http, $timeout) {
        $scope.taxObject  = {};
        $scope.serverData = {};

        $scope.saveTaxCodeKeypress = function(evt) {
            if (evt.keyCode == 13) {
                _animateProcess();
            }
        };

        $scope.saveTaxCodeClick = function() {
            _animateProcess();
        };

        /* Helpers */
        function _animateProcess() {
            var data,
                taxValue;

            data     = $scope.taxObject;
            taxValue = parseInt(data.taxcode);

            if (!isNaN(taxValue)) {
                $scope.serverData.taxCode = taxValue;
                data.taxcode = '';
                data.status = true;
                data.loader = true;
                data.statusCode = '';

                _saveDataToServer(data);
            }
            else {
                data.taxcode = '';
                data.status = false;
                data.statusCode = 'Niste vnesli številske vrednosti ali pa je napaka v davčni številki!';
            }
        }

        function _saveDataToServer(animObj) {
            $http.post('insert.php', $scope.serverData)
                .success(function(response) {
                    if (!!response) {
                        animObj.statusCode = 'Shranjeno!';
                        $timeout(function() {
                            animObj.loader = false;
                        }, 3400);
                    }
                });
        }
    }

    function CSVInputController($scope, $http, $timeout) {
        $scope.inputField = false;
        $scope.csvObject = {};
        $scope.fd = {};

        $scope.csvObject.loader = false;
        $scope.fd = new FormData();

        $scope.uploadFileToServer = function() {
            if ($scope.csvObject.file.type == 'text/csv') {
                if ($scope.csvObject.file.name) {
                    $scope.csvObject.loader = true;

                    _saveFileToServer();
                }
            }
            else {
                $scope.csvObject.status = false;
                $scope.csvObject.statusCode = 'Format datoteke ni pravilen!';
            }
        };

        function _saveFileToServer() {
            $http.post('add.php', $scope.fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
                .success(function(response) {
                    if (!!response) {
                        $scope.fd = new FormData();

                        $scope.csvObject.status     = true;
                        $scope.csvObject.statusCode = 'Uspešno dodane nove davčne številke iz dokumenta ' + $scope.csvObject.file.name + '!';
                        $timeout(function() {
                            $scope.csvObject.file   = false;
                            $scope.inputField.files = false;
                            $scope.csvObject.loader = false;
                        }, 3400);
                    }
                }
            );
        }
    }

})(angular);