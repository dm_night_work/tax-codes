# Tax Seeker Application

## System requirements
1. LAMP
2. PHP 5.4 >=

### Setup
1. Install __node__, __npm__, __grunt__ and __bower__ globally
2. Clone repo
3. Create file __conf.php__ in root (setup file is written in lower section)
4. With console navigate in root of project and
5. Install libraries with __bower__ => run `bower install` or `sudo bower install`
6. Install __grunt__ modules => run `npm install` or `sudo npm install`

**SETTING UP CONF.PHP :**

1. create conf.php file in root of project
2. define constants that are essentials for proper working project

```
#!php
<?php

/* Page related stuff */
define('APP', 'Preveri podjetje!');
define('BASE_URL', 'http://localhost/~fly/davcna-seeker/'); // change to match your URL

/* MySQL */
define('HOST', 'localhost');
define('USER', 'your-db-username');
define('PASS', 'your-db-pass');
define('DB', 'your-db-name');

```