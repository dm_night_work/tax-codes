<?php
/**
 * Created by PhpStorm.
 * User: fly
 * Date: 01/12/15
 * Time: 17:14
 */

require('conf.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST))
    $_POST = json_decode(file_get_contents('php://input'), true);

$tax_code = $_POST['taxCode'];
$db = new mysqli(HOST, USER, PASS, DB);

if ($db->connect_error > 0) die('Unable to connect to database ['. $db->connect_error . ']');

$tax_code = mysqli_real_escape_string($db, $tax_code);
$sql = "INSERT INTO tax_codes (tax) VALUES ('$tax_code')";

if ($db->query($sql)) echo true;
else echo false;