<?php
/**
 * Created by PhpStorm.
 * User: fly
 * Date: 01/12/15
 * Time: 13:24
 */

require('conf.php');

if (!isset($_POST['tax-code'])) {
    header('Location:' . BASE_URL);
    die();
}

$tax_code = $_POST['tax-code'];
$db = new mysqli(HOST, USER, PASS, DB);
$status = false;

if ($db->connect_error > 0) die('Unable to connect to database ['. $db->connect_error . ']');

$sql = "SELECT id, tax FROM tax_codes WHERE tax='$tax_code'";

if (!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');

while ($row = $result->fetch_assoc()) {
    if ($row['tax'] == $tax_code) {
        $status = true;
    }
}
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= APP; ?> | Poizvedba</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- stylesheets -->
    <link rel="stylesheet" href="css/production/style.min.css">
</head>
<body>
<div class="davcna-page--size-container davcna-tools--container-centered davcna-tools--copy-ready">
    <header class="davcna-page--size-wrapper davcna-header--container">
        <h1><?= APP; ?> - Rezultati poizvedbe:</h1>
    </header>
    <main class="davcna-page--size-wrapper davcna-main--container davcna-tools--text-centered">
        <div class="davcna-main--form-control davcna-tools--container-centered">
            <?php if ($status) : ?>
            <h4>Uspeh!</h4>
            <p>Podjetje z davčno številko: <strong><?= $tax_code; ?></strong> obstaja v naši bazi.</p>
            <?php else: ?>
            <h4>Ni rezultatov!</h4>
            <?php endif; ?>
        </div>
        <a href="<?= BASE_URL; ?>" class="davcna-tools--btn davcna-tools--btn-sbmt davcna-tools--btn-link">Nazaj</a>
    </main>
    <footer>
        <p><!-- created by internetnestrani.si --></p>
    </footer>
</div>
</body>
</html>