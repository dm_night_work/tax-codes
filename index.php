<?php require('conf.php'); ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= APP; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- stylesheets -->
    <link rel="stylesheet" href="css/production/style.min.css">
</head>
<body>
<div class="davcna-page--size-container davcna-tools--container-centered davcna-tools--copy-ready">
    <header class="davcna-page--size-wrapper davcna-header--container">
        <h1><?= APP; ?></h1>
        <p>Dobrodošli v Preveri podjetje! aplikaciji.</p>
        <p>Za poizvedbo je potrbno le pretipkati davčno številko in pritisniti ENTER tipko ali pa potrditi z gumbom.</p>
    </header>
    <main class="davcna-page--size-wrapper davcna-main--container davcna-tools--text-centered">
        <form action="search.php" method="POST" class="davcna-main--form-control davcna-tools--container-centered">
            <div class="davcna-main--input-control__wrapper">
                <input type="number" name="tax-code" class="davcna-page--size-wrapper davcna-main--input-control" placeholder="Vnesi davčno številko" required>
            </div>
            <button type="submit" class="davcna-tools--btn davcna-tools--btn-sbmt davcna-tools--container-centered">Išči</button>
        </form>
    </main>
</div>
</body>
</html>