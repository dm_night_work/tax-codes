<?php
/**
 * Created by PhpStorm.
 * User: fly
 * Date: 01/12/15
 * Time: 14:48
 */

require('conf.php');
session_start();

$session_data = array();

if (!isset($_POST['user-email']) || !isset($_POST['user-password'])) {
    doAction(false, false, 'login.php');
};

$email = $_POST['user-email'];
$pass = $_POST['user-password'];
$db = new mysqli(HOST, USER, PASS, DB);

if ($db->connect_error > 0) die('Unable to connect to database ['. $db->connect_error . ']');

$sql = "SELECT id, email, password FROM users WHERE email='$email'";

if (!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');

if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        if (password_verify($pass, $row['password'])) {
            doAction($row['email'], true, 'admin.php');
        } else {
            doAction($row['email'], false, 'login.php');
        }
    }
} else {
    doAction(false, false, '/login.php');
}

function doAction($email, $status, $url) {
    $session_data['email'] = $email;
    $session_data['status'] = $status;
    $_SESSION['login'] = $session_data;

    header('Location:' . BASE_URL . $url);
    die();
}