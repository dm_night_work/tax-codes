<?php
/**
 * Created by PhpStorm.
 * User: fly
 * Date: 01/12/15
 * Time: 14:36
 */

require('conf.php');
session_start();

$data = false;
$status = false; // FALSE - new request, TRUE - not new
if (isset($_SESSION['login'])) {
    $data = $_SESSION['login'];
    $status = true;
};
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= APP; ?> | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- stylesheets -->
    <link rel="stylesheet" href="css/production/style.min.css">
</head>
<body>
<div class="davcna-page--size-container davcna-tools--container-centered davcna-tools--copy-ready">
    <header class="davcna-page--size-wrapper davcna-header--container">
        <h1><?= APP; ?> - prijava v sistem</h1>
    </header>
    <main class="davcna-page--size-wrapper davcna-main--container davcna-tools--text-centered">
        <form action="login-check.php" method="POST" class="davcna-main--form-control davcna-tools--container-centered">
            <div class="davcna-main--input-control__wrapper">
                <input type="email" name="user-email" class="davcna-page--size-wrapper davcna-main--input-control" <?php if ($data['status'] && $data['email']) : ?>value="<?= $data['email']; ?>"<?php endif; ?> placeholder="Vnesi email naslov" required>
            </div>
            <div class="davcna-main--input-control__wrapper">
                <input type="password" name="user-password" class="davcna-page--size-wrapper davcna-main--input-control" placeholder="Vnesi geslo" required>
            </div>
            <button type="submit" class="davcna-tools--btn davcna-tools--btn-sbmt davcna-tools--container-centered">Prijava</button>
        </form>
        <?php if ($status) : ?>
            <?php if (!$data['email']) : ?>
                <p>Email naslov ni pravilen!</p>
            <?php else :
                if (!$data['status']) : ?>
                    <p>Geslo ni pravilno!</p>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
    </main>
</div>
</body>
</html>