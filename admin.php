<?php
/**
 * Created by PhpStorm.
 * User: fly
 * Date: 01/12/15
 * Time: 16:37
 */

require('conf.php');
session_start();

if (!$_SESSION['login']) {
    header('Location:' . BASE_URL . 'login.php');
    die();
}

$data = $_SESSION['login'];
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= APP; ?> | Administrativna plošča</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- stylesheets -->
    <link rel="stylesheet" href="css/production/style.min.css">
</head>
<body>
<div class="davcna-page--size-container davcna-tools--container-centered davcna-tools--copy-ready">
    <header class="davcna-page--size-wrapper davcna-header--container">
        <h1><?= APP; ?> - Administrativna plošča</h1>
        <p>Pozdravljen <strong><?= $data['email']; ?></strong> (<a href="logout.php">Odjava</a>)</p><br>
        <p>Nahajate se v administrativnem delu aplikacije.</p>
        <p>Tukaj je možno vnesti nove davčne številke v register.</p><br>
        <p>Možnosti:</p>
        <ol class="davcna-main--options-list">
            <li>Vnos davčne številke v polje in potrditev z klikom na gumb ali pa s tipko ENTER.</li>
            <li>Nalaganje CSV dokumentov. Davčne številke bodo dodane že obstoječim.</li>
        </ol>
    </header>
    <main class="davcna-page--size-wrapper davcna-main--container davcna-tools--text-centered" data-ng-app="TaxCodesApplication">
        <div data-ng-controller="SingleInputController" class="davcna-main--controller-wrapper">
            <h4>1. Vnesi posamično davčno številko</h4>
            <div class="davcna-main--form-control davcna-tools--container-centered">
                <div class="davcna-main--input-control__wrapper">
                    <input type="number" class="davcna-page--size-wrapper davcna-main--input-control" maxlength="8" placeholder="Vnesi davčno številko"
                           data-ng-model="taxObject.taxcode"
                           data-ng-keypress="saveTaxCodeKeypress($event)"
                           data-ng-minlength="8"
                           data-ng-maxlength="8"
                           required>
                </div>
                <button type="button" class="davcna-tools--btn davcna-tools--btn-sbmt davcna-tools--container-centered" data-ng-click="saveTaxCodeClick()">Potrdi</button>
            </div>
            <div class="davcna-tools--response-status"
                 data-ng-class="{'davcna-tools--response-status__success': taxObject.status, 'davcna-tools--response-status__error': !taxObject.status}"
                 data-ng-bind="taxObject.statusCode"></div>
            <div class="davcna-tools--loader-placeholder davcna-tools--deactivated"
                 data-ng-class="{'davcna-tools--loader-placeholder': taxObject.loader, 'davcna-tools--loader-placeholder davcna-tools--deactivated': !taxObject.loader}"></div>
        </div>
        <div data-ng-controller="CSVInputController" class="davcna-main--controller-wrapper">
            <h4>2. Naloži davčna števila iz CSV datoteke</h4>
            <div class="davcna-main--form-control davcna-tools--container-centered">
                <div class="davcna-main--input-control__wrapper">
                    <input type="file" data-grab-file="file">
                </div>
                <button type="button" class="davcna-tools--btn davcna-tools--btn-sbmt davcna-tools--container-centered" data-ng-click="uploadFileToServer()">Naloži</button>
            </div>
            <div class="davcna-tools--response-status"
                 data-ng-class="{'davcna-tools--response-status__success': csvObject.status, 'davcna-tools--response-status__error': !csvObject.status}"
                 data-ng-bind="csvObject.statusCode"></div>
            <div class="davcna-tools--loader-placeholder davcna-tools--deactivated"
                 data-ng-class="{'davcna-tools--loader-placeholder': csvObject.loader, 'davcna-tools--loader-placeholder davcna-tools--deactivated': !csvObject.loader}"></div>
        </div>
    </main>
</div>

<script src="js/production/script.min.js"></script>
</body>
</html>